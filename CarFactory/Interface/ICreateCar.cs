﻿using System;

namespace CarFactory
{
    public interface ICreateCar
    {
        public void Create();
    }
}
