﻿using System;

namespace CarFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            bool stop = true;
            do
            {
                Console.WriteLine("\n\n Select country of manufacture: \n 1 - Asia \t  2 - USA \t 3 - Europe \t 4 - Exit");
                string country = Console.ReadLine();
                switch (country)
                {
                    case "1":
                        {
                            Console.Clear();
                            AsiaFactory asia = new AsiaFactory();
                            asia.Create();
                            break;
                        }
                    case "2":
                        {
                            Console.Clear();
                            USAFactory usa = new USAFactory();
                            usa.Create();
                            break;
                        }
                    case "3":
                        {
                            Console.Clear();
                            EuropeFactory europe = new EuropeFactory();
                            europe.Create();
                            break;
                        }
                    case "4":
                        {
                            stop = false;
                            break;
                        }
                    default: Console.Clear();  break; 
                }

            }while(stop);
                
        }
    }
}
