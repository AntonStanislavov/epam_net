﻿using System;

namespace CarFactory
{
    public class BaseCar
    {
        public readonly Random random = new Random();

        protected string[] Engine = { "1.2", "1.4", "1.8", "2.0", "2.5", "3.0", "3.5" };
        protected enum Color { Black, White, Yellow, Red, Green }
        protected enum Fuel { Petrol, Diesel, LPG }
        protected enum Transmission { Automatic, Manual}
        protected enum Style { SUV, Hatchback, Coupe, Minivan, Sedan }
    }
}
