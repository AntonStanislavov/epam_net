﻿using System;


namespace CarFactory
{
    class USAFactory : BaseCar, ICreateCar
    {
        private enum Brand { Chevroleе, Cadillac, Dodge, Ford, GMC, Chrysler }
        public void Create()
        {
            var brand = (Brand)random.Next(0, 6);
            var engine = Engine[random.Next(0, Engine.Length)];
            var fuel = (Fuel)random.Next(0, 3);
            var transmission = (Transmission)random.Next(0, 2);
            var style = (Style)random.Next(0, 5);
            var color = (Color)random.Next(0, 5);
            Console.WriteLine("Your car is ready:");
            Console.WriteLine($"\n {brand} - {engine}L {fuel} - {transmission} - {style} - {color} ");
        }
    }
}
